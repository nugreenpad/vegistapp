package org.greenpad.vegist.anouncements;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import org.greenpad.vegist.OpenGLRenderer;
import org.greenpad.vegist.R;
import org.json.JSONArray;
import org.json.JSONObject;

public class AnounceActivity extends AppCompatActivity implements AnounceFragment.OnListFragmentInteractionListener {

    private TextView txt;
    private AnounceRetriever ar;
    private FrameLayout fr;
    private GLSurfaceView aSurfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anounce);

        txt = findViewById(R.id.loading);
        txt.setVisibility(View.VISIBLE);
        aSurfView = findViewById(R.id.aSurfView);
        aSurfView.setVisibility(View.VISIBLE);


        aSurfView.setBackground(getResources().getDrawable(R.drawable.bayterek));
        aSurfView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        aSurfView.getHolder().setFormat(PixelFormat.TRANSLUCENT);

        aSurfView.setZOrderOnTop(true);
        aSurfView.setEGLContextClientVersion(2);
        aSurfView.setRenderer(new OpenGLRenderer());


        fr = findViewById(R.id.anon_fragment);
        ar = new AnounceRetriever(this);
        ar.execute();

    }

    public void onAnounceLoaded(JSONArray arr){
        Log.e("ANON", "AnounceRetriever Finished");
        txt.setVisibility(View.GONE);
        aSurfView.setVisibility(View.GONE);
        fr.setVisibility(View.VISIBLE);
        Bundle bundle = new Bundle();
        bundle.putString("data", arr.toString());
        AnounceFragment af = new AnounceFragment();
        af.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.anon_fragment, af).commit();
    }

    @Override
    public void onListFragmentInteraction(JSONObject object) {
        String url = "null";
        try {
            url = object.getString("url");
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }catch (Exception x){
            x.printStackTrace();
        }
        Log.e("URL", url);
    }
}
