package org.greenpad.vegist.messaging;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Vibrator;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenpad.vegist.R;

/**
 * Created by root on 4/18/18.
 */

public class FBMessageHandler extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("MESSAGE", "From: " + remoteMessage.getFrom());


        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d("NOTIFICATION", "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Notification n = new Notification. Builder ( this )
                    . setSmallIcon (R. mipmap . ic_launcher )
                    . setContentTitle (remoteMessage.getNotification().getTitle())
                    . setContentText (remoteMessage.getNotification().getBody())
                    . setAutoCancel ( true )
                    . build();

            NotificationManager manager = (NotificationManager) getSystemService (
                    NOTIFICATION_SERVICE );
            Vibrator vib = (Vibrator) getSystemService (
                    Context. VIBRATOR_SERVICE );
            vib . vibrate (500);
            manager . notify ( 125 , n );
        }

    }
}
